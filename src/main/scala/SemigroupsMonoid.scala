import cats.Monoid
import cats.instances.int._ // for Monoid
import cats.instances.option._ // for Monoid
import cats.syntax.semigroup._ // for |+|

object SemigroupsMonoid extends App:
  println("SemigroupsMonoid")
  val option1 = Option(1)
  val option2 = Option(2)
  // Using map and getOrElse
  val result: Option[Int] = option1.map(x => x + option2.getOrElse(0))
  // Using Moniod with cats.instances.option
  val usingMonoid: Option[Int] = Monoid[Option[Int]].combine(option1, option2)
  println(usingMonoid)
  // Using Moniod with syntax in cats.syntax.semigroup with different types
  val usingMonoidSyntax: Option[Int] = option1 |+| option2
  val usingMonoidSyntax2: Option[String] = Option("A") |+| Option("B")
  val usingMonoidSyntax3: List[Int] = List(1, 2, 3) |+| List(4, 5, 6)
  println(usingMonoidSyntax) // Some(3)
  println(usingMonoidSyntax2) // Some(AB)
  println(usingMonoidSyntax3) // List(1, 2, 3, 4, 5, 6)
